<!DOCTYPE html>
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<title>Home</title>
	<!--Fuentes de google-->
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700" rel="stylesheet">

	<!-- Bootstrap-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!--Estilo css-->
	<link rel="stylesheet" href="../css/estilos.css" class="stylesheet">
</head>
<body class="fondoPE">
<!--menu de navegacion-->
	<header id="header">
		<nav class="menu">
		<!--Logo-->
			<div class="logo">
				<a href=""><img src="../img/pagina_expectativa/logocreinmus.png"></a>
				<a href="" class="btn-menu icono" id="btn-menu"><span class="glyphicon glyphicon-align-justify"></span></a>
			</div>
			<div class="enlaces" id="enlaces">
				
				<a href="index.html"><span class="glyphicon glyphicon-home"></span> Home</a>
				<a href="#registro"><span class="glyphicon glyphicon-pencil"></span> Registrate</a>
				<a href="#"><span class="glyphicon glyphicon-info-sign"></span> Conocenos</a>
				<a href="#"><span class="glyphicon glyphicon-envelope"></span> Contactanos</a>

			</div>

			<!-- -->
		</nav>

	</header>
	<!--aca empieza el contenedor con el contenido-->

	<br><br><br>
	<div class="container col-md-12 col-xs-12 ">
	
		<main>
			<article>
				<h1>Lorem ipsum dolor sit amet</h1>


				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda saepe tempora illo amet tenetur eveniet dolores corporis, alias odio, sunt vitae voluptas officia quisquam iusto perspiciatis eos hic expedita dolor.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda saepe tempora illo amet tenetur eveniet dolores corporis, alias odio, sunt vitae voluptas officia quisquam iusto perspiciatis eos hic expedita dolor.</p>


			</article>

		</main>	
			
		<main>
			<article>
				<h1>Lorem ipsum dolor sit amet</h1>


				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda saepe tempora illo amet tenetur eveniet dolores corporis, alias odio, sunt vitae voluptas officia quisquam iusto perspiciatis eos hic expedita dolor.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda saepe tempora illo amet tenetur eveniet dolores corporis, alias odio, sunt vitae voluptas officia quisquam iusto perspiciatis eos hic expedita dolor.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda saepe tempora illo amet tenetur eveniet dolores corporis, alias odio, sunt vitae voluptas officia quisquam iusto perspiciatis eos hic expedita dolor.</p>


			</article>
			
		</main>		
		
		
	</div>
	
	
	<!--ACa empieza el div de registro-->
	<div class="container" id="registro">
		<div class="row">
			<div class="col-md-offset-5 col-md-3 col-xs-12 formulario">
			<!--definicion del formulario-->
				<form action=""{{ route('register') }}" method="POST" name="formulario"  class="form-online">
                	<div class="form-login">
                		<br>
                		<h1 align="left" class="colorLetra"><strong>Registrate</strong></h1>
                		<h3 class="colorLetra">Lorem ipsum dolor sit amet, consectetur adipisicing eli</h3>
                		<br>
               			 <input align="center" required type="text" name="nombre" class="form-control input-sm chat-input entrada" placeholder="nombre" />
               			 </br>
                		<input  required type="email" name="correo" class="form-control input-sm chat-input entrada" placeholder="correo" />
               			 </br>
                		<div class="wrapper">
                			<span class="group-btn">  
                			<input type="checkbox" > <label for="">Deseo recibir noticias</label> <br>

               	   	 			<button  type="submit" name="enviar" class="btn btn-prymary btn-md" >Enviar <i class="fa fa-sign-in"></i></button><br><br>


                   			<!-- <a href="verificar_usuario.php" class="btn btn-primary btn-md">Entrar <i class="fa fa-sign-in"></i></a>-->
               			 </span>
                  	 
                	</div>
                </div>
               
            	</form><!--terminacion del formulario-->
			</div>
		</div>
	</div>
	<!--Aca termina el div de registro-->
<br><br>
<div class="clearfix"></div>
<div class="row ">
<div class="cols-xs-12 col-sm-6 col-md-2  fondoPE2 color">		


	</div>	
	<div class="cols-xs-12 col-sm-6 col-md-3  fondoPE2 color">
		<img src="../img/pagina_expectativa/OBCCQK0.png" width='550' height='550' align="center" alt="">


	</div>	
	<div class="cols-xs-12 col-sm-6 col-md-3 ">
		<div class="image">
			<img src="../img/pagina_expectativa/plantilla web creinmus-02.png" width='350' height='150' align="center" alt="">

		</div>
				
	</div>			


</div>

<footer>
	
	<h2 class="pie" align="center">©Lorem ipsum dolor sit ametst officiis?</h2>
</footer>
		<script src="../js/menu.js"></script>
		<script src="../js/headroom.min.js"></script>		
		
		
  		
		
</body>
</html>