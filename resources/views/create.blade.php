@extends('layout.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Nuevo usuario</h3>
		@if (count($errors)>0)
		<div class="alert alert-danger">
			<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		</div>
		@endif

		{!!Form::open(array('url'=>'usuario','method'=>'POST','autocomplete'=>'off'))!!}
		{{Form::token()}}
		<div class="form-group">
			<label for="nombre">Nombre</label>
			<input type="text" name="nombre" class="form-control" placeholder="Nombre...">	
		</div>
		<div class="form-group">
			<label for="apellidos">Apellidos</label>
			<input type="text" name="apellidos" class="form-control" placeholder="Apellidos...">	
		</div>
		<div class="form-group">
			<label for="correo">Correo</label>
			<input type="text" name="correo" class="form-control" placeholder="Correo...">	
		</div>
		<div class="form-group">
			<button class="btn btn-primary" type="submit">Guardar</button>
			<button class="btn btn-danger"  type="" >Cancelar</button>
		</div>
		{!!Form::close()!!}

	</div>

</div>	

	@endsection