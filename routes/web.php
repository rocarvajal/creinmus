<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These'
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {     return view('welcome'); });
Route::get('/espectativa', function () {     return view('espectativa'); });
Route::get('/nosotros', 'PaginaController@nosotros');
Route::get('/formulario','FormularioController@formulario');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/usuario','UsuarioController');