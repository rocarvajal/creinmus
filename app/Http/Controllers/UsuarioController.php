<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;



class UsuarioController extends Controller
{
    
    public function _construct (){

    }
    public function index (Request $request){

    	if ($request)
    	{
    		$query=trim($request->get('searchText'));
    		$usuario=DB::table('usuario')->where('nombre','LIKE','%'.$query.'%')
                //->where ('condicion','=','1')
                ->paginate(7);
    		return view('usuario',["usuario"=>$usuario,"searchText"=>$query]);
            
             
    	}
    }
    public function create (){
    	return view("create");
    }
	public function store (UsuarioFormRequest $request){

		$usuario=new Usuario;
		$usuario->nombre=$request->get('nombre');
		$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');
		//$usuario->condicion='1';
		$usuario->save();
		return Redirect::to('usuario');
    }
    public function show ($id){

    	return view("usuario.show",["usuario"=>Usuario::findOrFail($id)]);
    }
    public function edit ($id){

    	return view("edit",["usuario"=>Usuario::findOrFail($id)]);
    }
    public function update (UsuarioFormRequest $request,$id){

    	$usuario=Usuario::findOrFail($id);
    	/*$usuario->nombre=$request->get('nombre');
    	$usuario->apellidos=$request->get('apellidos');
        $usuario->correo=$request->get('correo');*/
        $usuario->update($request->all());
    	//$usuario->update;



    	return Redirect::to('usuario');
    }
    public function destroy ($id){

    	/*$usuario=Usuario::findOrFail($id);
    	$usuario->condicion='0';
    	$usuario->update();
        Session::flash('message','El usuario fue eliminado'):
        Usuario::destroy($id);*/
        return Redirect::to('usuario');
    }
}
